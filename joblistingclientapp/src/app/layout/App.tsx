import React, { useEffect, useMemo, useState } from "react";
import NavBar from "./NavBar";
import { ListGroup, ListGroupItem } from "react-bootstrap";
import { Joblisting } from "../models/joblisting";
import JobListingsDashboard from "../joblistings/JobListingsDashboard";
import { useApi } from "../api/useApi";
import { info } from "../utils/ToastContainer";
import { AddEditNewJobListingModal } from "../joblistings/AddEditNewJobListing";
import { ConfirmDeleteJobListing } from "./ConfirmDeleteJobListing";
import JobListingFilter from "../joblistings/JobListingFilter";

enum Modal {
  None,
  AddNewJobListing,
  ConfirmDeleteJobListing,
}

function App() {
  const api = useApi();
  let [jobListings, setJobListings] = useState<Joblisting[]>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingDetails, setIsLoadingDetails] = useState(false);
  const [selectedJobListing, setSelectedJobListing] = useState<
    Joblisting | undefined
  >(undefined);
  const [selectedJobId, setSelectedJobId] = useState("");
  const [modalId, setModalId] = useState(Modal.None);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);
  const [searchJob, setSearchJob] = useState<string>("");

  const handleSelectJobListing = (id: string | undefined) => {
    setIsLoadingDetails(true);
    setSelectedJobId(id ?? "");
    setSelectedJobListing(jobListings.find((job) => job?.id === id));
  };

  const closeModal = () => {
    setModalId(Modal.None);
    setIsEditMode(false);
    setSelectedJobListing(undefined);
  };

  const setIsJobDetailsLoaded = (value: boolean) => {
    setIsLoadingDetails(value);
  };

  const handleEditJobListing = (id: string | undefined) => {
    setIsEditMode(true);
    setSelectedJobListing(jobListings.find((job) => job?.id === id));
    setModalId(Modal.AddNewJobListing);
  };

  const filterJobListing = (id: string | undefined) => {
    setJobListings(jobListings.filter((_) => _.id !== id));
  };

  const handleAddJobForm = () => {
    setSelectedJobListing(undefined);
    setSelectedJobId("");
    setIsEditMode(false);
    setModalId(Modal.AddNewJobListing);
  };

  const deleteJobListing = (id?: string) => {
    setSelectedJobId("");
    if (!id) {
      info.error("Brak danych");
      return;
    }
    setSelectedJobId(id ?? "");

    setModalId(Modal.ConfirmDeleteJobListing);
  };

  const loadJobListing = () => {
    setIsLoading(true);
    api
      .getJobListings()
      .then((response) => {
        const selectedItem =
          response.filter((_) => _.id === selectedJobId) ?? [];
        const secondPart = response
          .filter((_) => _.id !== selectedJobId)
          .reverse();
        setJobListings(selectedItem.concat(secondPart));
        setIsLoading(false);
      })
      .catch((_) => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    loadJobListing();
  }, []);

  const filterJobListingByCriteria = useMemo(() => {
    if (!searchJob) {
      return jobListings;
    }

    return (jobListings = jobListings.filter(
      (_) =>
        _.title.toLowerCase().includes(searchJob.toLowerCase()) ||
        _.description.toLowerCase().includes(searchJob) ||
        _.category.includes(searchJob) ||
        _.city.includes(searchJob)
    ));
  }, [searchJob, jobListings]);

  const handleSearchJobListing = (value: string) => {
    value = value.trim();
    setSelectedJobListing(undefined);
    setSearchJob(value);
  };

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <header className="App-header">
          <NavBar openForm={handleAddJobForm} />
        </header>
      </nav>
      <div className="container mt-5">
        <div className="mt-5">
          <h3>Dostępne zadania</h3>
          <div className="col-12">
            <ListGroup>
              <ListGroupItem variant="info">
                Dostępnych zadań ({filterJobListingByCriteria.length})
              </ListGroupItem>
            </ListGroup>
            <JobListingFilter searchJobListing={handleSearchJobListing} />
            <JobListingsDashboard
              jobListings={filterJobListingByCriteria}
              isLoading={isLoading}
              selectJobListing={handleSelectJobListing}
              selectedJobListing={selectedJobListing}
              closeJobDetails={() => setSelectedJobListing(undefined)}
              jobIsLoading={isLoadingDetails}
              setIsLoadingDetails={setIsJobDetailsLoaded}
              selectedJobId={() => selectedJobId}
              handleDeleteJobListing={deleteJobListing}
              handleEditJobListing={handleEditJobListing}
            />
          </div>
        </div>
      </div>

      <AddEditNewJobListingModal
        show={modalId === Modal.AddNewJobListing}
        closeModal={closeModal}
        loadJobListing={loadJobListing}
        isEditMode={isEditMode}
        selectedJobListing={isEditMode ? selectedJobListing : undefined}
      />

      <ConfirmDeleteJobListing
        show={modalId === Modal.ConfirmDeleteJobListing}
        filterJobListing={filterJobListing}
        selectedJobId={selectedJobId}
        setIsJobDetailsLoaded={setIsJobDetailsLoaded}
        closeModal={closeModal}
        closeJobDetails={() => setSelectedJobListing(undefined)}
      />
    </>
  );
}

export default App;
