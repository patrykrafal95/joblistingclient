import React from "react";
import { Spinner } from "react-bootstrap";

interface Props {
  content?: string;
}

export default function LoadingComponent({ content = "Ładownie ..." }: Props) {
  function spinnerStyle() {
    return { height: "4rem", width: "4rem", fontSize: "2rem" };
  }

  return (
    <div className="d-flex justify-content-center mt-5">
      <Spinner animation="border" variant="danger" style={spinnerStyle()} />
    </div>
  );
}
