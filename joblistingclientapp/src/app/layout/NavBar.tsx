import React from "react";
import { Button, Navbar } from "react-bootstrap";

interface Props {
  openForm: () => void;
}

export default function NavBar({ openForm }: Props) {
  return (
    <Navbar bg="dark" variant="dark" fixed="top">
      <Button
        className="float-right"
        onClick={openForm}
        variant="outline-info"
        style={{ marginRight: "7em" }}
      >
        Add Job listing
      </Button>
    </Navbar>
  );
}
