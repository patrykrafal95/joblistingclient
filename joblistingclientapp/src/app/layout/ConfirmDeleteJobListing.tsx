import { info } from "../utils/ToastContainer";
import { useApi } from "../api/useApi";
import { Button, Modal } from "react-bootstrap";
import React from "react";

interface Props {
  selectedJobId: string | undefined;
  setIsJobDetailsLoaded: (value: boolean) => void;
  filterJobListing: (id: string | undefined) => void;
  show: boolean;
  closeModal: () => void;
  closeJobDetails: () => void;
}

export const ConfirmDeleteJobListing = ({
  selectedJobId,
  setIsJobDetailsLoaded,
  filterJobListing,
  show,
  closeModal,
  closeJobDetails,
}: Props) => {
  const api = useApi();

  const handleConfirmDelete = () => {
    setIsJobDetailsLoaded(true);
    api
      .deleteJobListing(selectedJobId)
      .then((_) => {
        info.success("Skasowano pomyślnie");
        setIsJobDetailsLoaded(false);
        filterJobListing(selectedJobId);
        closeJobDetails();
        closeModal();
      })
      .catch((_) => {
        setIsJobDetailsLoaded(false);
        closeJobDetails();
        closeModal();
      });
  };

  return (
    <Modal show={show} aria-labelledby="contained-modal-title-vcenter" centered>
      <Modal.Header closeButton onHide={closeModal}>
        <Modal.Title id="contained-modal-title-vcenter">
          Potwierdzenie
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>Czy na pewno chcesz skasować ?</Modal.Body>
      <Modal.Footer>
        <Button
          as="input"
          variant="outline-info"
          size="sm"
          type="submit"
          value="Zamknij"
          onClick={closeModal}
        />

        <Button
          as="input"
          variant="outline-danger"
          size="sm"
          type="submit"
          value="Kasuj"
          onClick={handleConfirmDelete}
        />
      </Modal.Footer>
    </Modal>
  );
};
