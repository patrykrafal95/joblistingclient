import React from "react";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

toast.configure({
  position: "bottom-left",
  autoClose: 5000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  theme: "light",
});

const error = (text: string) => {
  toast.error(text);
};

const success = (text: string) => {
  toast.success(text);
};

export const info = {
  error,
  success,
};
