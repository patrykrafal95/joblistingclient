export const DATE_TIME_FORMAT = "DD-MM-YYYY HH:mm:ss";
export const DATE_TO_SAVE_FORMAT = "YYYY-MM-DD";
export const DATE_TO_DISPLAY_FORMAT = "DD-MM-YYYY";
