import { Joblisting } from "../models/joblisting";
import React from "react";
import JobListingList from "./JobListing";
import LoadingComponent from "../layout/LoadingComponent";
import JobListingDetails from "./JobListingDetails";

interface Props {
  jobListings: Joblisting[];
  isLoading?: boolean;
  selectJobListing: (id: string | undefined) => void;
  selectedJobListing: Joblisting | undefined;
  closeJobDetails: () => void;
  jobIsLoading: boolean;
  setIsLoadingDetails: (value: boolean) => void;

  selectedJobId(): string;

  handleDeleteJobListing: (id: string | undefined) => void;
  handleEditJobListing: (id: string | undefined) => void;
}

export default function JobListingsDashboard({
  jobListings,
  isLoading,
  selectJobListing,
  selectedJobListing,
  closeJobDetails,
  jobIsLoading,
  setIsLoadingDetails,
  selectedJobId,
  handleDeleteJobListing,
  handleEditJobListing,
}: Props) {
  return (
    <div className="row">
      <div className={selectedJobListing ? "col-9" : "col-12"}>
        {isLoading ? (
          <LoadingComponent content={"Ładowanie... "} />
        ) : (
          <JobListingList
            jobListings={jobListings}
            selectJobListing={selectJobListing}
            selectedJobId={selectedJobId}
          />
        )}
      </div>
      {selectedJobListing ? (
        <div className="col-3">
          <JobListingDetails
            jobListing={selectedJobListing}
            closeJobDetails={closeJobDetails}
            isJobListing={jobIsLoading}
            setIsJobDetailsLoaded={setIsLoadingDetails}
            handleDeleteJobListing={handleDeleteJobListing}
            handleEditJobListing={handleEditJobListing}
          />
        </div>
      ) : (
        ""
      )}
    </div>
  );
}
