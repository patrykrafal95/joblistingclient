import { Joblisting } from "../models/joblisting";
import { Button, ListGroup, ListGroupItem } from "react-bootstrap";
import React from "react";
import { DATE_TO_DISPLAY_FORMAT } from "../utils/DateTimeFormat";
import moment from "moment/moment";

interface Props {
  jobListings: Joblisting[];
  selectJobListing: (id: string | undefined) => void;
  selectedJobId: () => string;
}

export default function JobListingList({
  jobListings,
  selectJobListing,
  selectedJobId,
}: Props) {
  function selectedJobListingId() {
    return { backgroundColor: "lightgreen", transition: ".5s ease-out" };
  }

  return (
    <ListGroup>
      {jobListings.length
        ? jobListings.map((jobListings) => (
            <ListGroupItem
              id={jobListings.id}
              key={jobListings.id}
              style={
                selectedJobId() === jobListings.id ? selectedJobListingId() : {}
              }
            >
              <h5>{jobListings.title}</h5>
              <p>{moment(jobListings.date).format(DATE_TO_DISPLAY_FORMAT)}</p>
              <p>{jobListings.category}</p>
              <Button
                as="input"
                variant="outline-dark"
                size="sm"
                type="submit"
                value="Detale"
                onClick={() => selectJobListing(jobListings?.id ?? "")}
              />{" "}
            </ListGroupItem>
          ))
        : "Nic nie znaleziono"}
    </ListGroup>
  );
}
