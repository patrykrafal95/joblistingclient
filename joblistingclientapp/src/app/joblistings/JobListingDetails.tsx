import { Joblisting } from "../models/joblisting";
import { Button, ListGroup, ListGroupItem } from "react-bootstrap";
import React from "react";
import LoadingComponent from "../layout/LoadingComponent";
import { DATE_TO_DISPLAY_FORMAT } from "../utils/DateTimeFormat";
import moment from "moment/moment";

interface Props {
  jobListing: Joblisting | undefined;
  closeJobDetails: () => void;
  isJobListing: boolean;
  setIsJobDetailsLoaded: (value: boolean) => void;
  handleDeleteJobListing: (id: string | undefined) => void;
  handleEditJobListing: (id: string | undefined) => void;
}

export default function JobListingDetails({
  jobListing,
  closeJobDetails,
  isJobListing,
  setIsJobDetailsLoaded,
  handleDeleteJobListing,
  handleEditJobListing,
}: Props) {
  let offsetTop =
    document.getElementById(jobListing?.id ? jobListing?.id : "")?.offsetTop ??
    0 + "px";

  let timer = setTimeout(() => {
    clearTimeout(timer);
    setIsJobDetailsLoaded(false);
  }, 100);

  return (
    <div style={{ position: "relative", top: offsetTop }}>
      {isJobListing ? (
        <LoadingComponent />
      ) : (
        <ListGroup>
          <ListGroupItem key={jobListing?.id}>
            <h5>{jobListing?.title}</h5>
            <p>{moment(jobListing?.date).format(DATE_TO_DISPLAY_FORMAT)}</p>
            <p>{jobListing?.city}</p>
            <p>{jobListing?.category}</p>
            <p>{jobListing?.description}</p>
            <Button
              as="input"
              variant="outline-dark"
              size="sm"
              type="submit"
              value="Edytuj"
              onClick={() => handleEditJobListing(jobListing?.id)}
            />{" "}
            <Button
              as="input"
              variant="outline-danger"
              size="sm"
              type="submit"
              value="Kasuj"
              onClick={() => {
                handleDeleteJobListing(jobListing?.id);
              }}
            />{" "}
            <Button
              as="input"
              variant="outline-info"
              size="sm"
              type="submit"
              value="Zamknij"
              onClick={closeJobDetails}
            />{" "}
          </ListGroupItem>
        </ListGroup>
      )}
    </div>
  );
}
