import React, { useEffect, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import { Joblisting } from "../models/joblisting";
import { nameof } from "../utils/GetPropName";
import { useApi } from "../api/useApi";
import { info } from "../utils/ToastContainer";
import moment from "moment";
import { DATE_TO_SAVE_FORMAT } from "../utils/DateTimeFormat";
import "react-inputs-validation/lib/react-inputs-validation.min.css";
import { isFalsy } from "utility-types";
import {
  SAVED_ERROR,
  SAVED_SUCCESS,
  UPDATED_SUCCESS,
} from "../const/globalMessages";

interface Props {
  show: boolean;
  closeModal: () => void;
  loadJobListing: () => void;
  isEditMode: boolean;
  selectedJobListing: Joblisting | undefined;
}

export const AddEditNewJobListingModal = ({
  show,
  closeModal,
  loadJobListing,
  isEditMode,
  selectedJobListing,
}: Props) => {
  const initState = {
    category: "",
    city: "",
    date: "",
    description: "",
    id: "",
    title: "",
  };

  const api = useApi();
  const [jobListing, setJobListing] = useState<Joblisting | undefined>(
    selectedJobListing ?? initState
  );

  useEffect(() => {
    setJobListing(selectedJobListing);
  }, [selectedJobListing]);

  const [invalidFields, setInvalidFields] = useState<Joblisting | undefined>(
    undefined
  );

  const [isSaving, setIsSaving] = useState<boolean>(false);

  const handleInputChange = (event: any) => {
    let { name, value } = event.target;
    setJobListing({ ...(jobListing ?? initState), [name]: value });
    console.log(jobListing);
  };
  const handleAddJob = () => {
    if (jobListing) {
      api
        .addJobListing(jobListing)
        .then((_) => {
          info.success(SAVED_SUCCESS);
          setIsSaving(false);
          setJobListing(initState);
          closeModal();
          loadJobListing();
          setInvalidFields(undefined);
        })
        .catch((_) => {
          info.error(SAVED_ERROR);
          setIsSaving(false);
        });
    }
  };

  const handleUpdateJob = () => {
    if (jobListing) {
      api
        .updateJobListing(jobListing)
        .then((_) => {
          info.success(UPDATED_SUCCESS);
          setIsSaving(false);
          setJobListing(initState);
          closeModal();
          loadJobListing();
          setInvalidFields(undefined);
        })
        .catch((_) => {
          info.error(SAVED_ERROR);
          setIsSaving(false);
        });
    }
  };

  const handleOnSubmit = () => {
    setIsSaving(true);

    let hasError: boolean = false;
    setInvalidFields(undefined);

    if (isFalsy(jobListing?.title)) {
      hasError = true;
      setInvalidFields((prevState) => ({
        ...(prevState ?? initState),
        [nameof<Joblisting>("title")]: "Podaj tytuł.",
      }));
    }

    if (isFalsy(jobListing?.description)) {
      hasError = true;
      setInvalidFields((prevState) => ({
        ...(prevState ?? initState),
        [nameof<Joblisting>("description")]: "Podaj opis.",
      }));
    }

    if (isFalsy(jobListing?.category)) {
      hasError = true;
      setInvalidFields((prevState) => ({
        ...(prevState ?? initState),
        [nameof<Joblisting>("category")]: "Podaj kategorie.",
      }));
    }

    if (isFalsy(jobListing?.city)) {
      hasError = true;
      setInvalidFields((prevState) => ({
        ...(prevState ?? initState),
        [nameof<Joblisting>("city")]: "Podaj miasto.",
      }));
    }

    if (isFalsy(jobListing?.date)) {
      hasError = true;
      setInvalidFields((prevState) => ({
        ...(prevState ?? initState),
        [nameof<Joblisting>("date")]: "Podaj date.",
      }));
    }

    if (hasError) {
      setIsSaving(false);
      return;
    }

    setIsSaving(true);

    if (isEditMode) {
      handleUpdateJob();
    } else {
      handleAddJob();
    }
  };

  return (
    <Modal show={show} aria-labelledby="contained-modal-title-vcenter" centered>
      <Modal.Header
        closeButton
        onHide={() => {
          closeModal();
          setJobListing(initState);
          setIsSaving(false);
          setInvalidFields(undefined);
        }}
      >
        <Modal.Title id="contained-modal-title-vcenter">
          {isEditMode ? "Edytuj" : "Dodaj"} nowe zadanie
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleOnSubmit}>
          <Form.Group controlId="jobListing.title">
            <Form.Label>Tytuł</Form.Label>
            <Form.Control
              size="sm"
              value={jobListing?.title}
              name={nameof<Joblisting>("title")}
              placeholder="Podaj tytuł"
              onChange={handleInputChange}
              isInvalid={!!invalidFields?.title}
            />
          </Form.Group>
          <Form.Group controlId="jobListing.description">
            <Form.Label>Opis</Form.Label>
            <Form.Control
              size="sm"
              value={jobListing?.description}
              name={nameof<Joblisting>("description")}
              placeholder="Podaj opis"
              onChange={handleInputChange}
              isInvalid={!!invalidFields?.description}
            />
          </Form.Group>
          <Form.Group controlId="jobListing.date">
            <Form.Label>Data</Form.Label>
            <Form.Control
              size="sm"
              type="date"
              value={moment(jobListing?.date ?? null).format(
                DATE_TO_SAVE_FORMAT
              )}
              name={nameof<Joblisting>("date")}
              placeholder="Wybierz date"
              onChange={handleInputChange}
              isInvalid={!!invalidFields?.date}
            />
          </Form.Group>
          <Form.Group controlId="jobListing.category">
            <Form.Label>Kategoria</Form.Label>
            <Form.Control
              size="sm"
              value={jobListing?.category}
              name={nameof<Joblisting>("category")}
              placeholder="Podaj kategorie"
              onChange={handleInputChange}
              isInvalid={!!invalidFields?.category}
            />
          </Form.Group>
          <Form.Group controlId="jobListing.city">
            <Form.Label>Miasto</Form.Label>
            <Form.Control
              size="sm"
              value={jobListing?.city}
              name={nameof<Joblisting>("city")}
              placeholder="Podaj miasto"
              onChange={handleInputChange}
              isInvalid={!!invalidFields?.city}
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="warning"
          size="sm"
          type="submit"
          onClick={() => {
            closeModal();
            setIsSaving(false);
            setJobListing(initState);
            setInvalidFields(undefined);
          }}
        >
          Zamknij
        </Button>
        <Button
          variant="danger"
          size="sm"
          type="submit"
          onClick={handleOnSubmit}
          disabled={isSaving}
        >
          {isSaving ? "Zapisywanie..." : "Zapisz"}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
