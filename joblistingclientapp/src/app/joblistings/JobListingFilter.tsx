import { CloseButton, Form, InputGroup } from "react-bootstrap";
import { ChangeEvent, useState } from "react";

interface Props {
  searchJobListing: (value: string) => void;
}

export default function JobListingFilter({ searchJobListing }: Props) {
  const [value, setValue] = useState<string>("");

  return (
    <>
      <Form>
        <Form.Group className="mt-3 mb-2">
          <Form.Label>Wyszukaj zadanie</Form.Label>
          <InputGroup>
            <Form.Control
              placeholder="Wprowadź fraze"
              value={value}
              onChange={(e: ChangeEvent<HTMLInputElement>) => {
                setValue(e.target.value);
                searchJobListing(e.target.value);
              }}
            />
            <InputGroup.Append
              onClick={() => {
                searchJobListing("");
                setValue("");
              }}
            >
              <InputGroup.Text>
                <CloseButton style={{ fontSize: "1.3em" }} />
              </InputGroup.Text>
            </InputGroup.Append>
          </InputGroup>
        </Form.Group>
      </Form>
    </>
  );
}
