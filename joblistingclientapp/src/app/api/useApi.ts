import { useContext } from "react";
import { context } from "./context";

export const useApi = () => {
  return useContext(context);
};
