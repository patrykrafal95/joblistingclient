import { createContext } from "react";
import { Api } from "./JobListingApi";

export const context = createContext<Api>(new Api());
