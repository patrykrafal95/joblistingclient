import axios, { AxiosInstance, AxiosResponse } from "axios";
import { Joblisting } from "../models/joblisting";
import { ApiError } from "./ApiError";
import { info } from "../utils/ToastContainer";
const DEFAULT_TIMEOUT = 5000;
const API_URL = "https://localhost:44321/api";

export class Api {
  private getRequestCreator(): AxiosInstance {
    const instance = axios.create({
      baseURL: API_URL,
      timeout: DEFAULT_TIMEOUT,
    });

    instance.interceptors.response.use(
      function (config) {
        return config;
      },
      function (error) {
        info.error("Wystąpił problem podczas połączenia z serwerem");
        console.log(error);
        return Promise.reject(error);
      }
    );
    return instance;
  }

  private handleError(e: any) {
    if (
      e.response?.data?.error &&
      typeof e.response.data.error.message === "string"
    ) {
      throw new ApiError(e.response.data.error.message, e.response.data.error);
    }

    throw e;
  }

  responseBody = <T>(response: AxiosResponse<T>) => response.data;

  requests = {
    get: <T>(url: string) => this.getRequestCreator().get<T>(url),
    post: <T>(url: string, body: {}) =>
      this.getRequestCreator().post<T>(url, body),
  };

  public async getJobListings(): Promise<Joblisting[]> {
    const response = await this.requests.get<Promise<Joblisting[]>>(
      "/JobListings"
    );
    try {
      if (Array.isArray(response.data)) {
        return response.data;
      }
    } catch (e) {
      this.handleError(e);
    }
    return [];
  }

  public async deleteJobListing(id: string | undefined): Promise<boolean> {
    try {
      await this.requests.post<Promise<boolean>>(
        "/JobListings/DeleteJobListing",
        {
          id,
        }
      );
      return true;
    } catch (e) {
      this.handleError(e);
    }
    return false;
  }

  public async addJobListing({
    title,
    description,
    date,
    category,
    city,
  }: Joblisting): Promise<boolean> {
    try {
      await this.requests.post<Promise<boolean>>("/JobListings/AddJobListing", {
        title,
        description,
        date,
        category,
        city,
      });

      return true;
    } catch (e) {
      this.handleError(e);
    }
    return false;
  }

  public async updateJobListing({
    id,
    title,
    description,
    date,
    category,
    city,
  }: Joblisting): Promise<boolean> {
    try {
      await this.requests.post<Promise<boolean>>(
        "/JobListings/UpdateJobListing",
        {
          id,
          title,
          description,
          date,
          category,
          city,
        }
      );

      return true;
    } catch (e) {
      this.handleError(e);
    }
    return false;
  }
}
